package helpers;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

public class SyncHelper {
    private static final String FALCON_SYNC_BASE_URL = FalconConfiguration.getString("application.syncBaseUrl");

    public Exception StartSync() {
        try {
            Unirest.setTimeouts(60000, 60000*10 - 10000);

            HttpResponse<String> response = Unirest.get(FALCON_SYNC_BASE_URL + "/falcon/startBlockingSync")
                    .header("Content-Type", "application/json").asString();


            String responseAsString = response.getBody();
            if(responseAsString.equalsIgnoreCase("sync finished")) {
                return null;
            }

            return new Exception(responseAsString);
        } catch (Exception ex) {
            return ex;
        }
    }

}
