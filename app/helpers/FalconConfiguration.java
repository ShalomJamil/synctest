package helpers;

import play.Configuration;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FalconConfiguration {
    @Inject
    static Configuration configuration;

    private FalconConfiguration() {
    }

    // returns env variable, else JVM argument (-D) else key in application.conf
    public static String getString(String key) {
        if (System.getenv(key) == null) {
            if (System.getProperty(key) == null) {
                return configuration.getString(key);
            } else {
                return System.getProperty(key);
            }
        } else {
            return System.getenv(key);
        }
    }

    public static int getInt(String key) {
        if (!System.getenv().containsKey(key)) {
            if (!System.getProperties().containsKey(key)) {
                return configuration.getInt(key);
            } else {
                return Integer.parseInt(System.getProperty(key));
            }
        } else {
            return Integer.parseInt(System.getenv(key));
        }
    }
}
