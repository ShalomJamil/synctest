package helpers;

import com.mashape.unirest.http.Unirest;

public class UserManagementHelper extends BasicFalconServersHelper {
    private String token = null;

    public Exception login() {
        try {
            response = Unirest.post(FalconConfiguration.getString("application.userManagementBaseUrl") + "/login")
                    .header("Content-Type", "application/json")
                    .header("x-verification-token", "5aF45abAGA7bva0031B878p7")
                    .header("x-verification-application", "Falcon")
                    .body("{\n    \"email\":\"" + FalconConfiguration.getString("credentials.falconUsername") +
                            "\",\n    \"password\":\"" + FalconConfiguration.getString("credentials.falconPassword") +
                            "\",\n    \"applicationId\":\"Falcon\"\n}")
                    .asJson();

            token = getStringData();
            if(token != null) {
                return null;
            }
            return new Exception("Invalid response - " + response.getBody().toString());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public String getToken() {
        return token;
    }
}
