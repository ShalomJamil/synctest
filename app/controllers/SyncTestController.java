package controllers;

import helpers.*;
import play.mvc.Result;
import play.mvc.Controller;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class SyncTestController extends Controller {
    public static final String SUCCESS_STRING = "Success";

    private static UserManagementHelper userManagementHelper = new UserManagementHelper();
    private static BackendHelper backendHelper = new BackendHelper();
    private static ClarizenHelper clarizenHelper = new ClarizenHelper();
    private static SyncHelper syncHelper = new SyncHelper();

    private String suffix;

    public Result getReadiness() {
        return ok("SyncTest is ready!");
    }

    public Result getLiveness() {
        return ok("SyncTest is alive!");
    }



    public Result userManagementLoginTest() {
        suffix = FalconConfiguration.getString("application.env") + " " +
                ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT);

        Exception ex = userManagementHelper.login();
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result clarizenLoginTest() {
        Exception ex = clarizenHelper.login();
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendConnectTest() {
        Exception ex = backendHelper.connect(userManagementHelper.getToken());
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendCreateWsTest() {
        Exception ex = backendHelper.createWorkspace("Workspace " + suffix);
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendSetWsSyncTest() {
        Exception ex = backendHelper.setWorkspaceSync();
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendCreateBoardTest() {
        Exception ex = backendHelper.createBoard("Test Board");
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendCreateMsTest() {
        Exception ex = backendHelper.createMilestone("Milestone " + suffix,
                ZonedDateTime.now(ZoneOffset.UTC).plusDays(30).format(DateTimeFormatter.ISO_INSTANT));
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendCreateTaskTest() {
        Exception ex = backendHelper.createTask("Task " + suffix);
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result startSyncTest() {
        if(!FalconConfiguration.getString("application.env").equalsIgnoreCase("prod")) {
            Exception ex = syncHelper.StartSync();
            if (ex == null) {
                return ok(SUCCESS_STRING);
            }
            return internalServerError(ex.getMessage());
        } else {
            try {
                Thread.currentThread().wait((long)60000 * 9);
                return ok(SUCCESS_STRING);
            } catch (Exception ex) {
                return internalServerError(ex.getMessage());
            }
        }
    }

    public Result validateSyncWsCreationTest() {
        Map<String, String> where = new HashMap<>();
        where.put("name", backendHelper.getWsName());

        return validateOneEntity(ClarizenHelper.EntityType.Project, where);
    }

    public Result validateSyncMsCreationTest() {
        Map<String, String> where = new HashMap<>();
        where.put("name", backendHelper.getMsName());

        return validateOneEntity(ClarizenHelper.EntityType.Milestone, where);
    }

    public Result validateSyncTaskCreationTest() {
        Map<String, String> where = new HashMap<>();
        where.put("name", backendHelper.getTaskName());

        return validateOneEntity(ClarizenHelper.EntityType.Task, where);
    }

    public Result backendDeleteMsTest() {
        Exception ex = backendHelper.deleteMilestone();
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendDeleteTaskTest() {
        Exception ex = backendHelper.deleteTask();
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }


    public Result validateSyncMsDeletionTest() {
        Map<String, String> where = new HashMap<>();
        where.put("name", backendHelper.getMsName());

        Exception ex = clarizenHelper.isNoEntityExists(ClarizenHelper.EntityType.Milestone, where);
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result validateSyncTaskDeletionTest() {
        Map<String, String> where = new HashMap<>();
        where.put("name", backendHelper.getTaskName());

        Exception ex = clarizenHelper.isNoEntityExists(ClarizenHelper.EntityType.Task, where);
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

    public Result backendDeleteWsTest() {
        Exception ex = backendHelper.deleteWorkspace();
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }


    public Result validateSyncWsNotDeletedTest() {
        Map<String, String> where = new HashMap<>();
        where.put("name", backendHelper.getWsName());

        return validateOneEntity(ClarizenHelper.EntityType.Project, where);
    }



    private Result validateOneEntity(ClarizenHelper.EntityType type, Map<String, String> where) {
        Exception ex = clarizenHelper.isOnlyOneEntityExists(type, where);
        if (ex == null) {
            return ok(SUCCESS_STRING);
        }
        return internalServerError(ex.getMessage());
    }

}
