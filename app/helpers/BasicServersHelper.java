package helpers;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

abstract class BasicServersHelper {
    static final String CONTENT_TYPE_HEADER_KEY;
    static final String APPLICATION_JSON_HEADER_VALUE;

    HttpResponse<JsonNode> response = null;

    static {
        CONTENT_TYPE_HEADER_KEY = "Content-Type";
        APPLICATION_JSON_HEADER_VALUE = "application/json";
    }

    JsonNode getJsonResponse() {
        if(response != null) {
            return response.getBody();
        }
        return new JsonNode("");
    }
}
