import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import controllers.SyncTestController;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.assertj.core.api.Assertions.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SyncTest {

    private String callTest(int testNumber) {
        HttpResponse<String> response;
        try {
            Unirest.setTimeouts(60000, 60000*10);
            response = Unirest.get("http://localhost:9006/sync/startTest" + testNumber)
                    .header("Content-Type", "application/json").asString();
            return response.getBody();
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @BeforeClass
    public static void setDefaultTimeouts() {

    }

    @Test
    public void t001UserManagementLoginTest() {
        assertThat(callTest(1)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t002ClarizenLoginTest() {
        assertThat(callTest(2)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t003BackendConnectTest() {
        assertThat(callTest(3)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t004BackendCreateWsTest() {
        assertThat(callTest(4)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t005BackendSetWsSyncTest() {
        assertThat(callTest(5)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t006BackendCreateBoardTest() {
        assertThat(callTest(6)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t007BackendCreateMsTest() {
        assertThat(callTest(7)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t008BackendCreateTaskTest() {
        assertThat(callTest(8)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t009StartSyncTest() {
        assertThat(callTest(9)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t010ValidateSyncWsCreationTest() {
        assertThat(callTest(10)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t011ValidateSyncMsCreationTest() {
        assertThat(callTest(11)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t012ValidateSyncTaskCreationTest() {
        assertThat(callTest(12)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t013BackendDeleteMsTest() {
        assertThat(callTest(13)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t014BackendDeleteTaskTest() {
        assertThat(callTest(14)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t015StartSyncTest() {
        assertThat(callTest(9)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t016ValidateSyncMsDeletionTest() {
        assertThat(callTest(15)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t017ValidateSyncTaskDeletionTest() {
        assertThat(callTest(16)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t018BackendDeleteWsTest() {
        assertThat(callTest(17)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t019StartSyncTest() {
        assertThat(callTest(9)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }

    @Test
    public void t020ValidateSyncWsNotDeletedTest() {
        assertThat(callTest(18)).isEqualTo(SyncTestController.SUCCESS_STRING);
    }
}
