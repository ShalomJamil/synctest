package helpers;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

public class ClarizenHelper extends BasicServersHelper {
    private String serverLocation = null;//api*
    private String appLocation = null;//app*
    private String sessionId = null;

    public enum EntityType {
        Task,
        Project,
        Milestone
    }

    public Exception login() {
        try {
            String username = FalconConfiguration.getString("credentials.clarizenUsername");
            String password = FalconConfiguration.getString("credentials.clarizenPassword");
            if(getServerDefinition(username, password)) {
                response = Unirest.post(serverLocation + "/authentication/login")
                        .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                        .body("{\n  \"username\":\"" + username + "\", \n  \"password\":\"" + password + "\"\n}")
                        .asJson();

                JSONObject jsonObject = getJsonResponse().getObject();
                if (jsonObject.has("sessionId")) {
                    sessionId = jsonObject.getString("sessionId");
                    return null;
                }
                return new Exception("Clarizen login failed (sessionId not found) - " + getJsonResponse());
            }
            return new Exception("Clarizen getServerDefinition failed");
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    private boolean getServerDefinition(String username, String password) throws UnirestException {
        response = Unirest.post("https://api.clarizen.com/V2.0/services/authentication/getServerDefinition")
                .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                .body("{\n  \"username\":\"" + username + "\", \n  \"password\":\"" + password + "\"\n}")
                .asJson();

        JSONObject jsonObject = getJsonResponse().getObject();
        if (jsonObject.has("serverLocation") && jsonObject.has("appLocation")) {
            serverLocation = jsonObject.getString("serverLocation");
            appLocation = jsonObject.getString("appLocation");
            return true;
        }
        return false;
    }

    public Exception isOnlyOneEntityExists(EntityType type, Map<String, String> where) {
        try {
            response = Unirest.post(serverLocation + "/data/entityQuery")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header("Authorization", "Session " + sessionId)
                    .body("{\n    \"typeName\": \"" + type.toString() + "\",\n    \"paging\": {\"limit\": 1}" + createWhereStatement(where) + "\n}")
                    .asJson();

            JSONObject jsonObject = getJsonResponse().getObject();
            if (jsonObject.has("entities") && jsonObject.has("paging")) {
                JSONArray entities = jsonObject.getJSONArray("entities");
                JSONObject paging = jsonObject.getJSONObject("paging");
                if (paging.has("hasMore") && entities.iterator().hasNext()) {
                    if (!paging.getBoolean("hasMore")) {
                        return null;
                    }

                    return new Exception("Clarizen isOnlyOneEntityExists failed (more then one entity was found) - " + getJsonResponse());
                } else {
                    return new Exception("Clarizen isOnlyOneEntityExists failed (entities[0]/hasMore not found) - " + getJsonResponse());
                }
            } else {
                return new Exception("Clarizen isOnlyOneEntityExists failed - " + getJsonResponse());
            }
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public Exception isNoEntityExists(EntityType type, Map<String, String> where) {
        try {
            response = Unirest.post(serverLocation + "/data/entityQuery")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header("Authorization", "Session " + sessionId)
                    .body("{\n    \"typeName\": \"" + type.toString() + "\",\n    \"paging\": {\"limit\": 1}" + createWhereStatement(where) + "\n}")
                    .asJson();

            JSONObject jsonObject = getJsonResponse().getObject();
            if (jsonObject.toString().startsWith("{\"entities\":[]")) {
                return null;
            }

            return new Exception("Clarizen isOnlyOneEntityExists failed (entities/paging not found) - " + getJsonResponse());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    private String createWhereStatement(Map<String, String> where) {
        if(where != null && !where.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(", where: { \"and\": [");
            int i=0;
            for(Map.Entry<String, String> entry : where.entrySet()) {
                i++;
                sb.append(createWhereCondition(entry.getKey(), entry.getValue()));
                if(i != where.size()) {
                    sb.append(", ");
                }
            }
            sb.append("]}");
            return sb.toString();
        }
        return "";
    }

    private String createWhereCondition(String field, String value) {
        return "{ \"leftExpression\":{ \"fieldName\": \"" + field + "\"}," +
                " \"operator\": \"Equal\"," +
                " \"rightExpression\": { \"value\": \"" + value + "\" } }";
    }
}
