package helpers;

import org.json.JSONObject;

abstract class BasicFalconServersHelper extends BasicServersHelper {
    boolean isSuccessful() {
        if(response != null) {
            JSONObject jsonObject = response.getBody().getObject();
            if(jsonObject != null && jsonObject.has("success")) {
                String success = jsonObject.get("success").toString();
                return success != null && success.equalsIgnoreCase("true");
            }
        }
        return false;
    }

    String getStringData() {
        if (isSuccessful()) {
            JSONObject jsonObject = response.getBody().getObject();
            if (jsonObject != null && jsonObject.has("data")) {
                return jsonObject.getString("data");
            }
        }
        //LoggerUtils.error("Failed to get String data " + getJsonResponse());
        return null;
    }

    JSONObject getJsonObjectData() {
        if (isSuccessful()) {
            JSONObject jsonObject = response.getBody().getObject();
            if (jsonObject != null && jsonObject.has("data")) {
                return jsonObject.getJSONObject("data");
            }
        }
        //LoggerUtils.error("Failed to get JsonObject data " + getJsonResponse());
        return null;
    }


}
