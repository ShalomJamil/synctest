package helpers;

import com.mashape.unirest.http.Unirest;
import org.json.JSONObject;

public class BackendHelper extends BasicFalconServersHelper {
    private static final String FALCON_SERVER_BASE_URL = FalconConfiguration.getString("application.falconServerBaseUrl");
    private static final String SESSION_ID_HEADER_KEY;

    private String sessionId = null;
    private Integer accountId = null;
    private Integer workspaceId = null;
    private Integer boardId = null;
    private Integer milestoneId = null;
    private Integer taskId = null;
    private String wsName = null;
    private String msName = null;
    private String taskName = null;

    static {
        SESSION_ID_HEADER_KEY = "SessionId";
    }

    public Exception connect(String umToken) {
        try {
            response = Unirest.post(FALCON_SERVER_BASE_URL + "/users/connect")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .body("{\n    \"token\":\"" + umToken + "\",\n    \"email\":\"" + FalconConfiguration.getString("credentials.falconUsername") + "\"\n}")
                    .asJson();

            JSONObject data = getJsonObjectData();
            if(data != null && data.has("sessionId") && data.has("accountId")) {
                sessionId = data.getString("sessionId");
                accountId = data.getInt("accountId");
                return null;
            }

            return new Exception("BE connect failed (data/sessionId/accountId not found) - " + getJsonResponse());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public Exception createWorkspace(String name) {
        try {
            response = Unirest.post(FALCON_SERVER_BASE_URL + "/workspaces")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{\n   \"accountId\":\"" + accountId + "\",\n   \"name\":\"" + name + "\" \n}")
                    .asJson();

            JSONObject data = getJsonObjectData();
            if(data != null && data.has("id")) {
                workspaceId = data.getInt("id");
                wsName = name;
                return null;
            }

            return new Exception("BE create workspace failed (data/id not found) - " + getJsonResponse());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public Exception setWorkspaceSync() {
        try {
            response = Unirest.put(FALCON_SERVER_BASE_URL + "/workspaces/sync")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{\n   \"id\":" + workspaceId + ",\n   \"isSync\":true \n}")
                    .asJson();

            if(isSuccessful()) {
                return null;
            }

            return new Exception("BE set workspace sync (data/id not found) - " + getJsonResponse());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public Exception deleteWorkspace() {
        try {
            response = Unirest.delete(FALCON_SERVER_BASE_URL + "/workspaces/" + workspaceId)
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{}")
                    .asJson();

            if(isSuccessful()) {
                return null;
            }

            return new Exception("Delete Workspace Failed");
        } catch (Exception ex) {
            return ex;
        } finally {
            workspaceId = null;
            response = null;
        }
    }

    public Exception createBoard(String name) {
        try {
            response = Unirest.post(FALCON_SERVER_BASE_URL + "/boards")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{\n   \"accountId\":\"" + accountId + "\",\n   \"workspaceId\":\"" + workspaceId + "\",\n   \"name\":\"" + name + "\"  \n}")
                    .asJson();

            JSONObject data = getJsonObjectData();
            if(data != null && data.has("id")) {
                boardId = data.getInt("id");
                return null;
            }
            return new Exception("BE create board failed (data/id not found) - " + getJsonResponse());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public boolean deleteBoard() {
        try {
            response = Unirest.delete(FALCON_SERVER_BASE_URL + "/boards/" + boardId)
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{}")
                    .asJson();

            return isSuccessful();
        } catch (Exception ex) {
            //LoggerUtils.error("BE delete board failed - " + getJsonResponse(), ex);
        } finally {
            boardId = null;
            response = null;
        }
        return false;
    }

    public Exception createMilestone(String name, String date) {
        try {
            response = Unirest.post(FALCON_SERVER_BASE_URL + "/milestones")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{\n    \"accountId\":\"" + accountId + "\",\n    \"workspaceId\":\"" + workspaceId + "\",\n    \"date\":\"" + date + "\",\n    \"name\":\"" + name + "\"   \n}")
                    .asJson();

            JSONObject data = getJsonObjectData();
            if(data != null && data.has("id")) {
                milestoneId = data.getInt("id");
                msName = name;
                return null;
            }

            return new Exception("BE create milestone failed (data/id not found) - " + getJsonResponse());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public Exception deleteMilestone() {
        try {
            response = Unirest.delete(FALCON_SERVER_BASE_URL + "/milestones/" + milestoneId)
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{}")
                    .asJson();

            if(isSuccessful()) {
                return null;
            }

            return new Exception("Delete Milestone Failed");
        } catch (Exception ex) {
            return ex;
        } finally {
            milestoneId = null;
            response = null;
        }
    }

    public Exception createTask(String name) {
        try {
            response = Unirest.post(FALCON_SERVER_BASE_URL + "/tasks")
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{\n    \"accountId\":\"" + accountId + "\",\n    \"workspaceId\":\"" + workspaceId + "\",\n    \"name\":\"" + name + "\"    \n}")
                    .asJson();

            JSONObject data = getJsonObjectData();
            if(data != null && data.has("id")) {
                taskId = data.getInt("id");
                taskName = name;
                return null;
            }
            return new Exception("BE create task failed (data/id not found) - " + getJsonResponse());
        } catch (Exception ex) {
            return ex;
        } finally {
            response = null;
        }
    }

    public Exception deleteTask() {
        try {
            response = Unirest.delete(FALCON_SERVER_BASE_URL + "/tasks/" + taskId)
                    .header(CONTENT_TYPE_HEADER_KEY, APPLICATION_JSON_HEADER_VALUE)
                    .header(SESSION_ID_HEADER_KEY, sessionId)
                    .body("{}")
                    .asJson();

            if(isSuccessful()) {
                return null;
            }

            return new Exception("Delete Task Failed");
        } catch (Exception ex) {
            return ex;
        } finally {
            taskId = null;
            response = null;
        }
    }

    public String getSessionId() {
        return sessionId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public Integer getWorkspaceId() {
        return workspaceId;
    }

    public Integer getBoardId() {
        return boardId;
    }

    public Integer getMilestoneId() {
        return milestoneId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public String getWsName() {
        return wsName;
    }

    public String getMsName() {
        return msName;
    }

    public String getTaskName() {
        return taskName;
    }
}
