[<img src="https://img.shields.io/travis/playframework/play-java-starter-example.svg"/>](https://travis-ci.org/playframework/play-java-starter-example)

# KPI micro service

This project provides connection from Eagle to external systems like Salesforce and Google Spreadsheets

## Running

Run this using [sbt](http://www.scala-sbt.org/).  

```
sbt swagger run
```

And then go to http://localhost:9000 to see the running web application.

## Swagger

To run swagger go to [local swagger url](http://localhost:9000/docs/swagger-ui/index.html?url=/assets/swagger.json)
