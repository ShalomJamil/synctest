import play.filters.cors.CORSFilter;
import play.http.DefaultHttpFilters;

import javax.inject.Inject;

public class CorsFilters extends DefaultHttpFilters {
    @Inject
    public CorsFilters(CORSFilter corsFilter) {
        super(corsFilter);
    }
}
