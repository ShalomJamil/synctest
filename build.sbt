name := """synctest"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, SwaggerPlugin, JavaAgent)

scalaVersion := "2.12.3"

libraryDependencies += guice
libraryDependencies += javaJpa
libraryDependencies += javaJdbc
//add support for cors
libraryDependencies += filters
//update db schema
libraryDependencies += evolutions

resolvers += "Clarizen" at "http://52.42.56.111:8081/nexus/content/repositories/snapshots/"

libraryDependencies += "dom4j" % "dom4j" % "1.6.1"
libraryDependencies += "org.webjars" % "swagger-ui" % "2.2.0"
//libraryDependencies += "com.google.code.gson" % "gson" % "2.8.2"

// Mongo
libraryDependencies += "org.mongodb.morphia" % "morphia" % "1.3.2"

// RLEUNG
libraryDependencies += "org.json" % "json" % "20160810"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.3"

//unirest
libraryDependencies += "com.mashape.unirest" % "unirest-java" % "1.4.9"

// Testing libraries for dealing with CompletionStage...
libraryDependencies += "org.assertj" % "assertj-core" % "3.6.2" % Test
libraryDependencies += "org.awaitility" % "awaitility" % "2.0.0" % Test
libraryDependencies += "com.intellij" % "annotations" % "12.0"

// Email Messaging
libraryDependencies += "com.clarizen" % "EagleMessaging" % "1.0.0-SNAPSHOT"
libraryDependencies += "com.sun.mail" % "javax.mail" % "1.5.6"
libraryDependencies += "commons-io" % "commons-io" % "2.5"

//logz.io
libraryDependencies += "io.logz.logback" % "logzio-logback-appender" % "1.0.17"

enablePlugins(DockerPlugin)
//Start Docker related part
val tag = {
  val buildNumber = System.getenv("TAG")
  if (buildNumber == null) {
    println("BUILD_NUMBER not defined, setting version: " + version.toString())
    "1.0-SNAPSHOT"
  } else {
    println("BUILD_NUMBER: " + buildNumber)
    new String(buildNumber)
  }
}

version in Docker := tag
maintainer in Docker := "Orr Chen <orr.hen@clarizen.com>"
dockerExposedPorts := Seq(9000)
dockerBaseImage := "java:8"
dockerEntrypoint := Seq("")
dockerRepository := Some("918989879316.dkr.ecr.eu-west-1.amazonaws.com/falcon")

// Make verbose tests
testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-v"))

swaggerDomainNameSpaces := Seq("api")

//support jpa db creation in prod
PlayKeys.externalizeResources := false

//additional docker commands
//dockerCommands += Seq(
//  Cmd("COPY","/target/swagger/swagger.json swagger/swagger.json")
//)

mappings in Universal += file("target/swagger/swagger.json") -> "assets/swagger.json"

javaOptions in Test += "-Dconfig.file=" + Option(System.getProperty("config.file")).getOrElse("conf/application.conf")